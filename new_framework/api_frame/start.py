import os
from api_frame.common.public import do_excel, Request, do_mysql, do_log
from api_frame.common.report import do_report

PATH = os.path.dirname(__file__)
s_path = os.path.join(PATH, 'datas', 'mysql.sql')
e_path = os.path.join(PATH, 'datas', 'test.xlsx')

# 初始化数据库
# do_mysql(s_path)

# 拿到excel数据
cases = do_excel(e_path)
R = Request()
# 执行前删除log.csv文件
os.system(f'del log.csv')
if os.path.exists('log.csv'):
    os.remove('log.csv')

for case in cases:
    try:
        res = R.do_request(case[0], case[1], case[2], data=case[3], type=case[4])
        exec(f"R.{case[5]}('{case[0]}',res,'{case[6]}')")
    except BaseException as e:
        # 如报错，写入错误信息
        do_log(case[0], 'error', str(e))
# 生成html报告
do_report('log.csv')

