import time


def do_report(path_csv, report_name="自动化测试报告"):
    # 读csv日志文件
    with open(path_csv, 'r', encoding='utf-8') as f:
        lines = f.readlines()

    # 用例数据
    pass_num = 0
    fail_num = 0
    error_num = 0
    for line in lines:
        if "success" in line:
            pass_num += 1
        elif "fail" in line:
            fail_num += 1
        elif "error" in line:
            error_num += 1
    total = pass_num+fail_num+error_num
    # html中用例情况
    start_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    table = ''
    num = 0
    for list in lines:
        num += 1
        li = list.split(',')

        table += '<tr align="center">\n'
        table += '<td >%s</td>\n'%num
        table += '<td >%s</td>\n'%li[0]
        table += '<td >%s</td>\n'%li[1]
        table += '<td >%s</td>\n'%li[2]
        table += "</tr>\n"

    # 生成html报告模板
    content = """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>TestHTML</title>
    </head>
    <body>
    <h1>%s</h1>
    <table>
        <tr><td valign="top">
            <h3>开始时间：%s</h3>
            <h3>用例总数：%s</h3>
            <h3>成功总数：%s</h3>
            <h3>失败总数：%s</h3>
            <h3>错误总数：%s</h3>
            </td>
        </tr>
    </table>
    <h3 >用例执行详情：</h3>
    <table border="1" >
        <tr bgcolor="green" align="center">
        <td width="10%%">用例编号</td>
        <td width="20%%">用例名称</td>
        <td width="20%%">执行结果</td>
        <td width="50%%">详细信息</td><tr>
        %s
    </table>
    </body>
    </html>
    """%(report_name,start_time,total,pass_num,fail_num,error_num,table)

    # 写入到html文件
    report_file = "report" + ".html"
    with open(report_file,"w",encoding="utf-8") as f:
        f.write(content)