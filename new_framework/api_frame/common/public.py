import os
from pprint import pprint

import requests
import xlrd


# 初始化mysql
def do_mysql(path):
    os.system(f'mysql -h192.149.2.149 -P3316 -uroot -p123456  -Dwoniusales<{path}')


# 拿到excle数据
def do_excel(path, name='Sheet1'):
    sheet = xlrd.open_workbook(path)
    data = sheet.sheet_by_name(name)
    n = data.nrows
    li = [data.row_values(i) for i in range(1, n)]
    return li


# 写日志
def do_log(name, result, msg=''):
    with open('log.csv', 'a+', encoding='utf-8') as f:
        f.write(name+','+result+','+msg+'\n')


class Request:

    def do_request(self,title, url, method, data=None,param=None, type=None):
        head = None
        if type in 'json':
            head = {'Content-Type':'application/json;charset=utf-8'}
        if type in 'form':
            head = {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}

        if method == 'get':
            param,data = data,param

        res = requests.request(method=method,url=url,params=param,data=data,headers=head)
        text = res.text
        print(f'--{title}：开始测试--')
        return text

    def assert_equal(self,title,result,expect):
        if result == expect:
            re = 'success'
            msg = ''
        else:
            re = 'fail'
            msg = f'{result} != {expect}'
        do_log(title, re, msg)

    def assert_in(self,title,result,expect):
        if result in expect:
            re = f'success'
            msg = ''
        else:
            re = f'fail'
            msg = f'{result} != {expect}'
        do_log(title, re, msg)


if __name__ == '__main__':
    cases = do_excel('../datas/test.xls')
    case = cases[0]
    pprint(cases)

    # R = Request()
    # res = R.do_request(case[0], case[1], case[2], case[3], type=case[4])
    # print(f'测试数据：{case}')
    # exec(f"R.{case[5]}(res,'{case[6]}')")
    pass

