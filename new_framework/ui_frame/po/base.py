from selenium import webdriver
from selenium.webdriver.support.expected_conditions import presence_of_element_located as EL
from selenium.webdriver.support.wait import WebDriverWait
from ui_frame.common.public import do_yaml
from selenium.webdriver.firefox.options import Options
import os


PATH = os.path.dirname(__file__)


class Base:
    locators = do_yaml(os.path.join(PATH, 'locator.yaml'))

    def __init__(self, browser='Firefox'):
        try:
            self.dr = getattr(webdriver, browser)()

        except Exception as e:
            print(e)
            op = Options()
            op.add_argument('-headless')
            self.dr = webdriver.Firefox(firefox_profile='', options=op)
        self.dr.get('http://www.baidu.com')

    def __getattr__(self, key):
        if key in self.locators.keys():
            element = WebDriverWait(self.dr, 10).until(EL(self.locators[key]))
            return element


if __name__ == '__main__':
    Base('Firefox')