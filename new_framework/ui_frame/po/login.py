from api_frame.common.public import do_log
from ui_frame.po.base import Base


class Login(Base):
    
    def __init__(self, title, browser='Firefox'):
        super().__init__(browser)
        self.title = title

    def login(self,name,pwd,verify):
        self.username.send_keys(name)
        self.password.send_keys(pwd)
        self.verify.send_keys(verify)
        self.login_button.click()

    # 断言
    def assert_equal(self,url_expect='http://192.172.2.149:8081/woniusales/sell'):
        url = self.dr.current_url

        if url == url_expect:
            re = 'success'
            msg = ''
        else:
            re = 'fail'
            msg = f'{url} != {url_expect}'
        do_log(self.title, re, msg)
        self.dr.close()

    def assert_not_equal(self,url_expect='http://192.172.2.149:8081/woniusales/sell'):
        url = self.dr.current_url
        if url != url_expect:
            re = 'success'
            msg = ''
        else:
            re = 'fail'
            msg = f'{url} == {url_expect}'
        do_log(self.title, re, msg)
        self.dr.close()


if __name__ == '__main__':
    a = Login('登陆')
    a.login('admin',123,'0000')
    a.assert_equal('http://192.172.2.149:8081/woniusales/sell')
