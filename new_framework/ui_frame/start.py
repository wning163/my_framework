import os
from ui_frame.common.public import do_excel, do_mysql, do_log
from ui_frame.common.reports import do_report
from ui_frame.common.public import do_step

PATH = os.path.dirname(__file__)
s_path = os.path.join(PATH, 'datas', 'mysql.sql')
e_path = os.path.join(PATH, 'datas', 'test.xlsx')

# 初始化数据库
# do_mysql(s_path)

# 拿到excel数据
cases = do_excel(e_path)

# 执行前删除log.csv文件
# os.system(f'del log.csv')
if os.path.exists('log.csv'):
    os.remove('log.csv')

for case in cases:
    # 导包 类实例化
    exec(f"from ui_frame.po.{case[1]} import {case[2]}")
    exec(f"obj={case[2]}('{case[0]}')")
    steps = do_step(case[3])
    # 执行步骤
    for step in steps:
        try:
            # print(f"obj.{step[0]}(*{step[1]})")
            exec(f"obj.{step[0]}(*{step[1]})")
        except BaseException as e:
            do_log(case[0], 'error', str(e))
do_report('log.csv')

