import time


def do_report(path_csv, report_name="自动化测试报告",pass_time='未知'):
    # 读csv日志文件
    with open(path_csv, 'r', encoding='utf-8') as f:
        lines = f.readlines()

    # 用例数据
    pass_num = 0
    fail_num = 0
    error_num = 0
    for line in lines:
        if "success" in line:
            pass_num += 1
        elif "fail" in line:
            fail_num += 1
        elif "error" in line:
            error_num += 1
    total = pass_num+fail_num+error_num
    name = '王宁'
    # html中用例情况
    start_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    table = ''
    num = 0
    for list in lines:
        num += 1
        li = list.split(',')

        table += '<tr align="center">\n'
        table += '<td >%s</td>\n'%num
        table += '<td >%s</td>\n'%li[0]
        table += '<td >%s</td>\n'%li[1]
        table += '<td >%s</td>\n'%li[2]
        table += "</tr>\n"

    # 生成html报告模板
    content = f"""
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title>测试报告</title>
        <meta name="generator" content="HTMLTestRunner 1.2.0"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
    
    <style type="text/css" media="screen">
    </style>
    </head>
   
    <body >
    <div style="margin-left:20px; margin-right:20px;">
    <div style="width: 650px; float: left; ">
        <h1 style="font-family: Microsoft YaHei">{report_name}</h1>
        <p class='attribute'><strong>测试人员 : </strong> {name}</p>
    <p class='attribute'><strong>开始时间 : </strong> {start_time}</p>
    <p class='attribute'><strong>合计耗时 : </strong> {pass_time}</p>
    <p class='attribute'><strong>测试结果 : </strong> 共 {total}，通过 {pass_num}，失败 {fail_num}，错误{error_num}，通过率 = {pass_num/total:%}</p>
    </div>
    
    
    <div style="width: 500px; clear: both;">
    <p id='show_detail_line'>
    <a class="btn btn-primary" href='javascript:showCase(0)'>通过率 {pass_num/total:.2%}</a>
    <a class="btn btn-success" href='javascript:showCase(2)'>通过{pass_num}</a>
    <a class="btn btn-danger" href='javascript:showCase(1)'>失败{fail_num}</a>
    <a class="btn btn-warning" href='javascript:showCase(3)'>错误{error_num}</a>
    <a class="btn btn-info" href='javascript:showCase(4)'>所有{total}</a>
    </p>
    </div>
    <table id='result_table' class="table table-condensed table-bordered table-hover">

        <tr bgcolor="#CCFF99" align="center">
        <td width="10%%">用例编号</td>
        <td width="20%%">用例名称</td>
        <td width="20%%">执行结果</td>
        <td width="50%%">详细信息</td><tr>
        {table}
    </tr>
    </table>
    </div>
    </body>
    </html>
        """

    # 写入到html文件
    report_file = "report" + ".html"
    with open(report_file,"w",encoding="utf-8") as f:
        f.write(content)