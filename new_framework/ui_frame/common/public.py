import os
import xlrd
import yaml


# 初始化mysql
def do_mysql(path):
    os.system(f'mysql -h192.149.2.149 -P3316 -uroot -p123456  -Dwoniusales<{path}')


# 拿到excle数据
def do_excel(path,name='Sheet1'):
    sheet = xlrd.open_workbook(path)
    data = sheet.sheet_by_name(name)
    n = data.nrows
    li = [data.row_values(i) for i in range(1, n)]
    return li


# 读yaml文件
def do_yaml(path):
    with open(path,'r') as f:
        data = yaml.safe_load(f)

        return data


# 写日志
def do_log(name,result,msg=''):
    with open('log.csv','a+',encoding='utf-8') as f:
        f.write(name+','+result+','+msg+'\n')


# excel中步骤处理  'login:admin,123456\nassert_login:成功'
# 处理后[['login', ['admin', '123456']], ['assert_login', ['成功']]]
def do_step(str: str):
    lists = []
    li = str.split('\n')
    for row in li:
        row_li = row.split(':', 1)

        data = []
        if row_li[1] != '':
            data = row_li[1].split(',')

        row_list = [row_li[0], data]
        lists.append(row_list)
    return lists


if __name__ == '__main__':
    # cases = do_excel('../datas/test.xlsx')
    # case = cases[0]
    #
    # R = Request()
    # res = R.do_request(case[0],case[1],case[2],case[3],type=case[4])
    # print(f'测试数据：{case}')
    # exec(f"R.{case[5]}(res,'{case[6]}')")
    data = do_yaml('../po/locator.yaml')
    print(data)

    pass

